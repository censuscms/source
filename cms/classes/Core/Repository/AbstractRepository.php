<?php
namespace CENSUS\Core\Repository;

/**
 * AbstractRepository
 *
 * author: Marc Scherer
 * package: census CMS
 */

abstract class AbstractRepository
{
	/**
	 * @var \CENSUS\Core\Model\AbstractModel;
	 */
	private $model = null;
}